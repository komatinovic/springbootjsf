package com.gmail.kom.marijana.zadatak.views;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import com.gmail.kom.marijana.zadatak.entities.Document;

@ManagedBean (name = "document", eager = true)
@RequestScoped
public class DocumentView extends Document{

	public DocumentView() {
		
	}
	
}
