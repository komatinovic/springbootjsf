package com.gmail.kom.marijana.zadatak.configuration;

import javax.faces.webapp.FacesServlet;

import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ConfigureJSF {

	@Bean
    public ServletRegistrationBean servletRegistrationBean() {

        FacesServlet servlet = new FacesServlet();
        ServletRegistrationBean servletRegistrationBean = new ServletRegistrationBean(servlet, "*.jsf");

        return servletRegistrationBean;
	}
	
}
