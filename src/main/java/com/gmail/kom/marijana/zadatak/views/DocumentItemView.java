package com.gmail.kom.marijana.zadatak.views;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import com.gmail.kom.marijana.zadatak.entities.DocumentItem;

@ManagedBean (name = "documentitem", eager = true)
@RequestScoped
public class DocumentItemView extends DocumentItem{

	public DocumentItemView() {
		
	}
	
}
