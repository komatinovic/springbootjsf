package com.gmail.kom.marijana.zadatak.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.gmail.kom.marijana.zadatak.entities.Document;
import com.gmail.kom.marijana.zadatak.entities.DocumentItem;

public interface DocumentItemRepository extends CrudRepository<DocumentItem, Long>{

	public List<DocumentItem> findByDocument(Document document);
}
