package com.gmail.kom.marijana.zadatak.beans;

import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;

import org.springframework.beans.factory.annotation.Autowired;

import com.gmail.kom.marijana.zadatak.entities.Document;
import com.gmail.kom.marijana.zadatak.entities.DocumentItem;
import com.gmail.kom.marijana.zadatak.repositories.DocumentItemRepository;
import com.gmail.kom.marijana.zadatak.repositories.DocumentRepository;
import com.gmail.kom.marijana.zadatak.views.DocumentItemView;
import com.gmail.kom.marijana.zadatak.views.DocumentView;

@ManagedBean (name = "jsfBean", eager = true)
@RequestScoped
public class JsfBean {

	@Autowired
	private DocumentRepository docRepo;
	
	@Autowired
	private DocumentItemRepository docItemRepo;
	
	@ManagedProperty(value = "#{document}")
    private DocumentView document = new DocumentView();
	
	@ManagedProperty(value = "#{documentitem}")
    private DocumentItemView documentItem = new DocumentItemView();

    public void resetDocument(){
    	this.document.setId(0);
    	this.document.setName(null);
    	this.document.setDate(null);
    	this.document.setCode(null);
    }
	
    public void resetDocumentItem(){
    	this.documentItem.setId(0);
    	this.documentItem.setName(null);
    	this.documentItem.setPrice(null);
    	this.documentItem.setDocument(null);
    }
    
    public List<Document> getAllDocuments(){
    	return (List<Document>) docRepo.findAll();
    }
    
    public void viewDocument(Document doc) {
    	doc = docRepo.findOne(this.document.getId());
        
        this.document.setId(doc.getId());
        this.document.setName(doc.getName());
        this.document.setDate(doc.getDate());
        this.document.setCode(doc.getCode());
    }
    
    public String addNewDocument(){
    	Document doc = new Document();
    	
    	doc.setCode(this.document.getCode());
    	doc.setDate(this.document.getDate());
    	doc.setName(this.document.getName());
    	
    	docRepo.save(doc);
    	
    	return "documents.xhtml";
    }
    
    public String deleteDocument(Document doc) {
    	this.document.setId(doc.getId());
        
        docRepo.delete(doc);
    	
    	return "documents.xhtml";
    }
    
    public String editDocument() {
    	Document doc = new Document();
    	
    	doc.setId(this.document.getId());
    	doc.setCode(this.document.getCode());
    	doc.setDate(this.document.getDate());
    	doc.setName(this.document.getName());
    	
    	docRepo.save(doc);
    	
    	return "viewdocument.xhtml?faces-redirect=true&id=" + doc.getId();
    }
    
    public List<DocumentItem> getAllDocumentItems(){
    	Document doc = new Document();
    	
    	doc.setId(this.document.getId());
    	doc.setCode(this.document.getCode());
    	doc.setDate(this.document.getDate());
    	doc.setName(this.document.getName());
    	
    	return (List<DocumentItem>) docItemRepo.findByDocument(doc);
    }
    
    public void viewDocumentItem(){
    	DocumentItem docItem = docItemRepo.findOne(this.documentItem.getId());
        
        this.documentItem.setName(docItem.getName());
        this.documentItem.setPrice(docItem.getPrice());
        this.documentItem.setDocument(docItem.getDocument());
    }
    
    public String addNewDocumentItem(Document doc){
    	DocumentItem docItem = new DocumentItem();
    	
    	docItem.setName(this.documentItem.getName());
    	docItem.setPrice(this.documentItem.getPrice());
    	
    	doc = docRepo.findOne(this.document.getId());
    	docItem.setDocument(doc);
    	
    	docItemRepo.save(docItem);
    	
    	return "viewdocument.xhtml?faces-redirect=true&id=" + doc.getId();
    }
    
    public String deleteDocumentItem(DocumentItem docItem) {
    	
    	docItemRepo.delete(docItem);
    	
    	return "viewdocument.xhtml?faces-redirect=true&id=" + docItem.getDocument().getId();
    }
    
    public String editDocumentItem(){
    	DocumentItem docItem = new DocumentItem();
    	
    	docItem.setId(this.documentItem.getId());
    	docItem.setName(this.documentItem.getName());
    	docItem.setPrice(this.documentItem.getPrice());
    	docItem.setDocument(this.documentItem.getDocument());
    	
    	docItemRepo.save(docItem);
    	
    	return "viewdocument.xhtml?faces-redirect=true&id=" + docItem.getDocument().getId();
    }
    
    

	public DocumentItemView getDocumentItem() {
		return documentItem;
	}

	public void setDocumentItem(DocumentItemView documentItem) {
		this.documentItem = documentItem;
	}

	public DocumentRepository getDocRepo() {
		return docRepo;
	}

	public void setDocRepo(DocumentRepository docRepo) {
		this.docRepo = docRepo;
	}

	public DocumentItemRepository getDocItemRepo() {
		return docItemRepo;
	}

	public void setDocItemRepo(DocumentItemRepository docItemRepo) {
		this.docItemRepo = docItemRepo;
	}

	public DocumentView getDocument() {
		return document;
	}

	public void setDocument(DocumentView document) {
		this.document = document;
	}
}
