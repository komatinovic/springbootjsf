package com.gmail.kom.marijana.zadatak.repositories;

import org.springframework.data.repository.CrudRepository;

import com.gmail.kom.marijana.zadatak.entities.Document;

public interface DocumentRepository extends CrudRepository<Document, Long>{

}
