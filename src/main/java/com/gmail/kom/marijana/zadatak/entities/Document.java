package com.gmail.kom.marijana.zadatak.entities;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
public class Document{

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long id;
	
	private String code;
	
	private Date date;
	
	private String name;
	
	@JsonManagedReference
	@OneToMany (fetch=FetchType.EAGER,
			cascade=CascadeType.REMOVE,
			mappedBy="document")
	private List<DocumentItem> items = new ArrayList<>();
	
	public Document() {
		// TODO Auto-generated constructor stub
	}
	
	public Document(String code, Date date, String name) {
		super();
		this.code = code;
		this.date = date;
		this.name = name;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<DocumentItem> getItems() {
		return items;
	}

	public void setItems(List<DocumentItem> items) {
		this.items = items;
	}
	
	@Override
	public String toString() {
		return name;
	}
}
