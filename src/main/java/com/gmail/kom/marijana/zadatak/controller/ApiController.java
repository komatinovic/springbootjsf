package com.gmail.kom.marijana.zadatak.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.gmail.kom.marijana.zadatak.entities.Document;
import com.gmail.kom.marijana.zadatak.entities.DocumentItem;
import com.gmail.kom.marijana.zadatak.repositories.DocumentItemRepository;
import com.gmail.kom.marijana.zadatak.repositories.DocumentRepository;

@RestController
@RequestMapping("/api")
public class ApiController {
	
	@Autowired
	private DocumentRepository documentRepo;
	
	@Autowired
	private DocumentItemRepository documentItemRepo;

	@GetMapping("/documents")
	public List<Document> getAllDocs(){
		return (List<Document>) documentRepo.findAll();
	}
	
	@GetMapping("/documents/{id}")
	public Document getDocument(@PathVariable long id){
	  return documentRepo.findOne(id);
	}
	
	@PostMapping("/documents/add")
	public Document addDocument(Document d){
	  return documentRepo.save(d);
	}
	
	@PostMapping("documents/delete/{id}")
	public void deleteDocument(@PathVariable long id){
		documentRepo.delete(id);
	}
	
	@PostMapping("/documents/update")
	public Document updateDocument(Document d){
	  return documentRepo.save(d);
	}
	
	@GetMapping("/documents/{id}/items")
	public List<DocumentItem> getAllItems(@PathVariable long id){
		Document doc = documentRepo.findOne(id);
		return doc.getItems();
	}
	
	@GetMapping("/documents/{id}/items/{item_id}")
	public DocumentItem getItem(@PathVariable long id, @PathVariable long item_id){
		return documentItemRepo.findOne(item_id);
	}
	
	@PostMapping("/documents/{id}/items/add")
	public DocumentItem addDocumentItem(@PathVariable long id, DocumentItem dItem){
		dItem.setDocument(documentRepo.findOne(id));
		return documentItemRepo.save(dItem);
	}
	
	@PostMapping("/documents/{id}/items/update")
	public DocumentItem updateDocumentItem(@PathVariable long id, DocumentItem dItem){
		dItem.setDocument(documentRepo.findOne(id));
		return documentItemRepo.save(dItem);
	}
	
	@PostMapping("/items/delete/{id}")
	public void deleteDocumentItem(@PathVariable long id){
		documentItemRepo.delete(id);
	}
}
