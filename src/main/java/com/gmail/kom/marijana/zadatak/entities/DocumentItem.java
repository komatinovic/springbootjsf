package com.gmail.kom.marijana.zadatak.entities;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
public class DocumentItem {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long id;
	
	private String name;
	
	private Double price;
	
	@JsonBackReference
	@ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name = "document_id")
	private Document document;
	
	public DocumentItem() {
		// TODO Auto-generated constructor stub
	}

	public DocumentItem(String name, Double price, Document document) {
		super();
		this.name = name;
		this.price = price;
		this.document = document;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Document getDocument() {
		return document;
	}

	public void setDocument(Document document) {
		this.document = document;
	}
	
}
